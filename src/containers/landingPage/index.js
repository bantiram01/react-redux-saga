import React from "react";
import { connect } from "react-redux";
import { bindActionCreators } from "redux";
import { fetchUsersInitiated } from "../../redux/user/actions";

class LandingPage extends React.Component {
	componentDidMount() {
		this.props.dispatch(fetchUsersInitiated());
	}

	renderRows() {
		const { searchResults } = this.props.user;
		return (
			searchResults &&
			searchResults.length > 0 &&
			searchResults.map((item, index) => {
				return (
					<tr>
						<td>{item.id}</td>
						<td>{item.employee_name}</td>
						<td>{item.employee_age}</td>
						<td>{item.employee_salary}</td>
					</tr>
				);
			})
		);
	}
	render() {
		return (
			<div>
				<table>
					<tr>
						<th>Employee id</th>
						<th>Employee name</th>
						<th>Age</th>
						<th>Salary</th>
					</tr>
					{this.renderRows()}
				</table>
			</div>
		);
	}
}

const mapStateToProps = state => {
	return {
		user: state.user
	};
};

export default connect(mapStateToProps, null)(LandingPage);
