import axios from "axios";

const baseUrl = "http://dummy.restapiexample.com/api/v1";

class RestClient {
  // get method
  static get(url, params) {
    return new Promise(function(resolve, reject) {
      axios
        .get(`${baseUrl}${url}`)
        .then(function(response) {
          resolve(response.data);
        })
        .catch(function(error) {
          reject(error);
        })
        .then(function() {
          // always executed
        });
    });
  }

  static delete(url, params) {
    return new Promise(function(resolve, reject) {
      axios
        .delete(`${baseUrl}${url}`)
        .then(function(response) {
          resolve(response.data);
        })
        .catch(function(error) {
          reject(error);
        })
        .then(function() {
          // always executed
        });
    });
  }

  /***************
    ......
    ......
    ......
    ....other api methods like post, delete, put goes here.....
    ......
    ......
    ......

  ************/
}

export default RestClient;
