import * as TYPE from "./types";
import _ from "underscore";
// initial state of the reducer
const initialState = {
	searchResults: []
};

export default function reducer(state = initialState, action) {
	switch (action.type) {
		case TYPE.USER_FETCH_SUCCEEDED:
			return { ...state, searchResults: action.data };

		case TYPE.DELETE_USER_REQUESTED:
			state.searchResults.splice(
				_.findIndex(state.searchResults, { id: action.payload.id }),
				1
			);
			return { ...state };
		default:
			return state;
	}
}
