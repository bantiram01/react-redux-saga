export const fetchUsersInitiated = data => ({
  type: "USER_FETCH_REQUESTED",
  payload: data
});
