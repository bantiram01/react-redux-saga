export const HANDLE_ERROR = "HANDLE_ERROR";
export const HIDE_LOADER = "HIDE_LOADER";
export const SHOW_LOADER = "SHOW_LOADER";
export const SWEETALERT_SHOW = "@SweetAlert/Show";
