import { persistCombineReducers } from "redux-persist";
import storage from "redux-persist/lib/storage";
import { reducers as swalReducers } from "react-redux-sweetalert2";
import { routerReducer } from "react-router-redux";
import encryptor from "./encryptor";
import compressor from "./compressor";
import app from "./app";
import user from "./user";

// store configurations
const storeConfig = {
	key: "primary",
	storage: storage,
	blacklist: ["app", "swal"],
	transforms: [encryptor, compressor]
};

// Combine all the reducers into one
const KlearNowTaskApp = persistCombineReducers(storeConfig, {
	app,
	user,
	routerReducer,
	swal: swalReducers
});

export default KlearNowTaskApp;
