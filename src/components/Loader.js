// Global loader for whole application //

import React from "react";
import { SyncLoader } from "halogenium";

export default ({ loading = true }) => {
	return (
		loading && (
			<div
				style={{
					display: "block",
					fontSize: "0",
					position: "fixed",
					zIndex: "9999",
					top: "50%",
					left: "50%",
					transform: "translate(-50%, -50%)"
				}}
			>
				<SyncLoader color="#007bff" size="25px" margin="4px" />
			</div>
		)
	);
};
