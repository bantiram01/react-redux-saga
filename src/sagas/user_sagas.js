import { call, put, takeEvery, takeLatest } from "redux-saga/effects";
import { swal } from "react-redux-sweetalert2";
import { push } from "react-router-redux";
import createHistory from "history/createBrowserHistory";
import RestClient from "../utilities/RestClient";

let history = createHistory();

// worker Saga: will be fired on USER_FETCH_REQUESTED actions
function* fetchUser(action) {
    try {
        yield put({ type: "SHOW_LOADER" });
        const users = yield call(RestClient.get, "/employees", {}); // first arg is restclient method, second is endpoint, third is params
        yield put({ type: "USER_FETCH_SUCCEEDED", data: users.data });
        if (users.status == "success") {
            yield put(
                swal.showAlert({
                    title: "Success!",
                    text: "Users successfully populated!",
                    type: "success",
                    confirmAlert: () => history.push("/")
                })
            );
        }
    } catch (e) {
        yield put({ type: "USER_FETCH_FAILED", message: e.message });
        yield put(
            swal.showAlert({
                title: "Error!",
                text: e.message,
                type: "error",
                confirmAlert: () => false
            })
        );
    }
}

// watcher saga
function* userSaga() {
    yield takeLatest("USER_FETCH_REQUESTED", fetchUser);
}

export default userSaga;
