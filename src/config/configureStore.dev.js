import { applyMiddleware, createStore } from "redux";
import { persistStore } from "redux-persist";
import { composeWithDevTools } from "redux-devtools-extension";
import createSagaMiddleware from "redux-saga";

import * as thunk from "redux-thunk";
import reducer from "../redux";
import { routerMiddleware } from "react-router-redux";
import rootSaga from "../sagas";
// configured store under a development mode
export default function configureStore(history) {
	const sagaMiddleware = createSagaMiddleware();
	const store = createStore(
		reducer,
		composeWithDevTools(
			applyMiddleware(sagaMiddleware, routerMiddleware(history))
		)
	);
	sagaMiddleware.run(rootSaga);
	const persistor = persistStore(store);

	return { persistor, store };
}
